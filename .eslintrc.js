module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'no-use-before-define': 0,
    '@typescript-eslint/no-use-before-define': ['error'],
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': 0,
    'react/jsx-filename-extension': 0,
    'import/no-unresolved': 0,
    'no-underscore-dangle': 1,
    'import/extensions': 0,
    'global-require': 0,
    '@typescript-eslint/no-unused-vars': 1,
    'arrow-body-style': 0,
  },
};
