import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import BlockItem from '../Elements/Block/BlockItem';
import { BlockType } from '../types/types';

import './firstBlocksStyles.css';

interface ComponentProps {
  blocksInf: BlockType[]
}

const FirstBlocksLayout = ({
  blocksInf,
}: ComponentProps) => {
  return (
    <div className="blocks first-blocks">
      <div className="blocks-wrapper scuare_blocks">
        {blocksInf.map((item) => (
          <BlockItem
            key={item.id}
            blockInf={item}
            backColorClassName={backColorByClassNames.gray}
          />
        ))}
      </div>
    </div>
  );
};

export default FirstBlocksLayout;
