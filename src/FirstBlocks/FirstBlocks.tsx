import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import { BlockType } from '../types/types';
import FirstBlocksLayout from './FirstBlocksLayout';

const confForBlocks: BlockType[] = [
  {
    id: 's4nvds',
    backColor: backColorByClassNames.gray,
    description: 'Гений непредвзято понимает под собой конфликт. Закон внешнего мира, очевидно, реально подчеркивает аксиоматичный здравый смысл. Культ джайнизма включает в себя поклонение Махавире и другим тиртханкарам, поэтому закон исключённого третьего индуцирует изоморфный ротор.',
  },
  {
    id: 'sc80jv',
    backColor: backColorByClassNames.gray,
    caption: 'Чувство однократно',
  },
  {
    id: 'sdfgs',
    backColor: backColorByClassNames.gray,
    caption: 'Морено',
  },
];

const FirstBlocks = () => {
  return (
    <FirstBlocksLayout
      blocksInf={confForBlocks}
    />
  );
};

export default FirstBlocks;
