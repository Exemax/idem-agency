import React from 'react';

import './chatMessageStyles.css';

interface ComponentProps {
  message: string
}

const ChatMessage = ({
  message,
}: ComponentProps) => {
  return (
    <p className="chat-message">
      {message}
    </p>
  );
};

export default ChatMessage;
