import React from 'react';
import cn from 'classnames';
import { BlockType } from '../../types/types';

import './blockStyle.css';
import UserInfoList from '../UserInfoList/UserInfoList';

interface BlockItemProps {
  blockInf: BlockType
  isTwoSized?: boolean
  backColorClassName: string
}

const BlockItem = ({
  blockInf,
  isTwoSized,
  backColorClassName,
}: BlockItemProps) => {
  const classNameBlock = cn('block', backColorClassName, {
    block_two_size: isTwoSized,
    block_text_white: blockInf.image,
  });

  const elementsForBlock = () => (
    <>
      {blockInf.description && (
        <p className="block__content__description">{blockInf.description}</p>
      )}
      {blockInf.caption && (
        <span className="block__content__caption">{blockInf.caption}</span>
      )}
      {blockInf.userInfo && (
        <UserInfoList userInfo={blockInf.userInfo} />
      )}
    </>
  );

  return (
    <div className={classNameBlock}>
      {blockInf.image && (
        <img src={blockInf.image} alt="blockImage" className="block__image" />
      )}
      <div className="block__content">
        {elementsForBlock()}
      </div>
    </div>
  );
};

BlockItem.defaultProps = {
  isTwoSized: false,
};

export default BlockItem;
