import React from 'react';
import { UserInfo } from '../../types/types';

import './userInfoListStyles.css';

interface UserInfoListProps {
  userInfo: UserInfo
}

const UserInfoList = ({
  userInfo,
}: UserInfoListProps) => {
  const {
    fullName,
    surname,
    policyNumber,
    numberPhone,
    birthDay,
  } = userInfo;

  return (
    <ul className="user-info-list">
      <li className="user-info-list__item">
        <span className="user-info-list__item__field">Фамилия</span>
        <span className="user-info-list__item__value">{surname}</span>
      </li>
      <li className="user-info-list__item">
        <span className="user-info-list__item__field">Имя и отчество</span>
        <span className="user-info-list__item__value">{fullName}</span>
      </li>
      <li className="user-info-list__item">
        <span className="user-info-list__item__field">Номер полиса выдан заведомо ложный</span>
        <span className="user-info-list__item__value">{policyNumber}</span>
      </li>
      <li className="user-info-list__item">
        <span className="user-info-list__item__field">Дата рождения</span>
        <span className="user-info-list__item__value">{birthDay}</span>
      </li>
      <li className="user-info-list__item">
        <span className="user-info-list__item__field">Телефон</span>
        <span className="user-info-list__item__value">{numberPhone}</span>
      </li>
    </ul>
  );
};

export default UserInfoList;
