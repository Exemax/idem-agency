import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import { BlockType } from '../types/types';
import ThirdBlocksLayout from './ThirdBlocksLayout';

import shipImage from '../images/picture/shipImage.jpg';
import largeShipImage from '../images/picture/shipImage@2x.jpg';

const confForBlocks: BlockType[] = [
  {
    id: 's4nvds',
    backColor: backColorByClassNames.green,
    userInfo: {
      surname: 'Мамай',
      fullName: 'Станиславаленина Владимировна',
      policyNumber: '01354879',
      birthDay: '01.12.1976',
      numberPhone: '79254218069',
    },
  },
  {
    id: 'sc80jv',
    backColor: backColorByClassNames.green,
    caption: 'Понятие модернизации понимает механизм власти',
  },
  {
    id: 'sdfgs',
    backColor: backColorByClassNames.green,
    caption: 'Наши сотрудники',
    image: shipImage,
    largeImage: largeShipImage,
  },
];

const ThirdBlocks = () => {
  return (
    <ThirdBlocksLayout
      blocksInf={confForBlocks}
    />
  );
};

export default ThirdBlocks;
