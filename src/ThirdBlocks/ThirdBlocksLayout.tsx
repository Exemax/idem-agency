import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import BlockItem from '../Elements/Block/BlockItem';
import { BlockType } from '../types/types';

import './thirdBlocksStyles.css';

interface ComponentProps {
  blocksInf: BlockType[]
}

const ThirdBlocksLayout = ({
  blocksInf,
}: ComponentProps) => {
  return (
    <div className="blocks third-blocks">
      <div className="blocks-wrapper scuare_blocks">
        {blocksInf.map((item) => (
          <BlockItem
            key={item.id}
            blockInf={item}
            backColorClassName={backColorByClassNames.green}
          />
        ))}
      </div>
    </div>
  );
};

export default ThirdBlocksLayout;
