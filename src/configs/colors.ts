export const backColorByClassNames = {
  gray: 'backcolor_gray',
  lightgray: 'backcolor_lightgray',
  blue: 'backcolor_blue',
  orange: 'backcolor_orange',
  green: 'backcolor_green',
  darkorange: 'backcolor_darkorange',
  red: 'backcolor_red',
  purple: 'backcolor_purple',
};
