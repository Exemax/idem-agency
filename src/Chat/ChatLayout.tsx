import React, { ReactChild } from 'react';

import './chatStyles.css';

interface ChatLayoutProps {
  children: ReactChild
}

const ChatLayout = ({
  children,
}: ChatLayoutProps) => {
  return (
    <div className="modal">
      <div className="chat">
        {children}
      </div>
    </div>
  );
};

export default ChatLayout;
