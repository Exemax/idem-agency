import cn from 'classnames';
import React from 'react';
import ChatMessage from '../Elements/ChatMessage/ChatMessage';
import { ChatType } from '../types/types';

interface ChatHistoryProps {
  chatHistory: ChatType[]
}

const RenderChatMessage = ({
  messageInf, messageNum,
}: {
  messageInf: ChatType, messageNum: number,
}) => {
  const currentUser = messageInf.userId === 'user1';

  return (
    <>
      {messageNum === 4 && (
        <div className="messages__date-point">21 октября 2017</div>
      )}
      <div className={cn('message-frame', { 'message-frame_curent-user': currentUser })}>
        <img src={messageInf.userPhoto} alt="avatar" className="message-frame__avatar" />
        <ChatMessage message={messageInf.message} />
      </div>
    </>
  );
};

const ChatHistory = ({
  chatHistory,
}: ChatHistoryProps) => {
  return (
    <div className="chat-history">
      {chatHistory.map((messageInf, n) => (
        <RenderChatMessage messageInf={messageInf} key={messageInf.id} messageNum={n} />
      ))}
    </div>
  );
};

export default ChatHistory;
