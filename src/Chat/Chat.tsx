import React from 'react';
import ChatLayout from './ChatLayout';
import avatarImg from '../images/avatars/avatar.jpg';
import avatarLargeImg from '../images/avatars/avatar@2x.jpg';
import { ChatType } from '../types/types';
import ChatHistory from './ChatHistory';
import ChatForm from './ChatForm';

const chatHistory: ChatType[] = [
  {
    id: '1',
    userId: 'user1',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'Это означает, что в Вашем доме, офисе, кабинете, мастерской хранятся ценности бóльшие, чем просто деньги или драгоценности, - культурные ценности. Произведения искусства, антиквариат, иные предметы коллекционирования – все это имущество, безусловно высокая стоимость которого.',
  },
  {
    id: '2',
    userId: 'user2',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'Это означает',
  },
  {
    id: '3',
    userId: 'user1',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'доме, офисе, кабинете, мастерской',
  },
  {
    id: '4',
    userId: 'user2',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'Это означает, что в Вашем доме, офисе, кабинете, мастерской хранятся ценности бóльшие, чем просто деньги или драгоценности, - культурные ценности. Произведения искусства, антиквариат, иные предметы коллекционирования – все это имущество, безусловно высокая стоимость которого. ',
  },
  {
    id: '5',
    userId: 'user1',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'Это означает',
  },
  {
    id: '6',
    userId: 'user2',
    userPhoto: avatarImg,
    userLargePhoto: avatarLargeImg,
    message: 'доме, офисе, кабинете, мастерской',
  },
];

const Chat = () => {
  const onSendMessage = () => {

  };

  return (
    <ChatLayout>
      <>
        <ChatHistory chatHistory={chatHistory} />
        <ChatForm onSendMessage={onSendMessage} />
      </>
    </ChatLayout>
  );
};

export default Chat;
