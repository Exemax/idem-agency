import { useFormik } from 'formik';
import React from 'react';
import { ChatType } from '../types/types';

interface ChatFormProps {
  onSendMessage: (values: Pick<ChatType, 'message'>) => void
}

const ChatForm = ({
  onSendMessage,
}: ChatFormProps) => {
  const chatForm = useFormik({
    initialValues: {
      message: '',
    },
    onSubmit: (values) => onSendMessage(values),
  });

  return (
    <form onSubmit={chatForm.handleSubmit} className="chat-form">
      <textarea
        name="message"
        value={chatForm.values.message}
        onChange={chatForm.handleChange}
        className="chat-form__text-area"
        placeholder="Enter message"
      />
      <button
        className="chat-form__submit-button"
        type="submit"
        disabled={chatForm.values.message === ''}
      >
        Отправить
      </button>
    </form>
  );
};

export default ChatForm;
