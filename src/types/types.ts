type Caption = string;
type Description = string;
type Image = string;
type Id = string
type BackColor = string

type UserSurname = string
type UserFullName = string
type PolicyNumber = string
type BirthDay = string
type NumberPhone = string

export type UserInfo = {
  surname: UserSurname
  fullName: UserFullName
  policyNumber: PolicyNumber
  birthDay: BirthDay
  numberPhone: NumberPhone
}

export type BlockType = {
  id: Id
  caption?: Caption
  description?: Description
  image?: Image
  largeImage?: Image
  backColor: BackColor
  userInfo?: UserInfo
}

export type ChatType = {
  id: Id
  userId: Id
  userPhoto: Image
  userLargePhoto: Image
  message: Description
}
