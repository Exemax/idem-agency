import React from 'react';
import { Route, Switch } from 'react-router';
import Chat from '../Chat';
import MainPage from '../MainPage/MainPage';

const publickUrl = process.env.PUBLIC_URL;

const Routes = () => {
  return (
    <Switch>
      <Route path={`${publickUrl}/`} exact>
        <MainPage />
      </Route>
      <Route path={`${publickUrl}/chat`}>
        <Chat />
      </Route>
    </Switch>
  );
};

export default Routes;
