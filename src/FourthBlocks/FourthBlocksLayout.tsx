import React from 'react';
import BlockItem from '../Elements/Block/BlockItem';
import { BlockType } from '../types/types';

import './fourthBlocksStyles.css';

interface ComponentProps {
  blocksInf: BlockType[]
}

const FourthBocksLayout = ({
  blocksInf,
}: ComponentProps) => {
  return (
    <div className="fourth-blocks blocks-wrapper">
      {blocksInf.map((item, n) => (
        <BlockItem
          blockInf={item}
          key={item.id}
          isTwoSized={n === 1}
          backColorClassName={item.backColor}
        />
      ))}
    </div>
  );
};

export default FourthBocksLayout;
