import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import { BlockType } from '../types/types';
import FourthBocksLayout from './FourthBlocksLayout';

const confForBlocks: BlockType[] = [
  {
    id: '14nv1s',
    backColor: backColorByClassNames.darkorange,
    caption: 'Лидерство',
  },
  {
    id: '2c802v',
    caption: 'Однако',
    backColor: backColorByClassNames.darkorange,
  },
  {
    id: '3df3s',
    backColor: backColorByClassNames.red,
    caption: 'Невероятный гений',
  },
  {
    id: '4jjos',
    backColor: backColorByClassNames.darkorange,
    caption: 'Точка перегиба',
  },
  {
    id: '52sPso',
    backColor: backColorByClassNames.purple,
    caption: 'Длина вектора',
  },
];

const FourthBocks = () => {
  return (
    <FourthBocksLayout
      blocksInf={confForBlocks}
    />
  );
};

export default FourthBocks;
