import React from 'react';
import Layout from '../Layout';
import MainPageLayout from './MainPageLayout';

const MainPage = () => {
  return (
    <Layout>
      <MainPageLayout />
    </Layout>
  );
};

export default MainPage;
