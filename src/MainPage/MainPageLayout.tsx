import React from 'react';
import FirstBlocks from '../FirstBlocks';
import FourthBlocks from '../FourthBlocks';
import SecondBlocks from '../SecondBlocks';
import ThirdBlocks from '../ThirdBlocks';

import './mainContentStyle.css';

const MainPageLayout = () => {
  return (
    <div className="main-content">
      <FirstBlocks />
      <SecondBlocks />
      <ThirdBlocks />
      <FourthBlocks />
    </div>
  );
};

export default MainPageLayout;
