import React from 'react';
import BlockItem from '../Elements/Block/BlockItem';
import { BlockType } from '../types/types';

import './secondBlocksStyles.css';

interface ComponentProps {
  blocksInf: BlockType[]
}

const SecondBlockLayout = ({
  blocksInf,
}: ComponentProps) => {
  return (
    <div className="second-blocks blocks-wrapper">
      {blocksInf.map((item, n) => (
        <BlockItem
          blockInf={item}
          key={item.id}
          isTwoSized={n === 3}
          backColorClassName={item.backColor}
        />
      ))}
    </div>
  );
};

export default SecondBlockLayout;
