import React from 'react';
import { backColorByClassNames } from '../configs/colors';
import { BlockType } from '../types/types';
import SecondBlockLayout from './SecondBlocksLayout';

const confForBlocks: BlockType[] = [
  {
    id: 's4nv1s',
    backColor: backColorByClassNames.lightgray,
    caption: 'Наши сотрудники',
  },
  {
    id: 'sc802v',
    backColor: backColorByClassNames.lightgray,
    caption: 'Просветляет понимающий тест',
  },
  {
    id: 'sdf3s',
    backColor: backColorByClassNames.blue,
    caption: 'Интроекция',
  },
  {
    id: '1jjos',
    backColor: backColorByClassNames.lightgray,
    caption: 'Конформизм',
  },
  {
    id: '32sPso',
    backColor: backColorByClassNames.orange,
    caption: 'Наши сотрудники',
  },
];

const SecondBlocks = () => {
  return (
    <SecondBlockLayout
      blocksInf={confForBlocks}
    />
  );
};

export default SecondBlocks;
