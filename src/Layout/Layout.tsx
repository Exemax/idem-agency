import React, { ReactChild } from 'react';
import Footer from './Footer';
import Header from './Header';

import './styles.css';

interface LayoutProps {
  children: ReactChild
}

const Layout = ({
  children,
}: LayoutProps) => {
  return (
    <div className="page">
      <Header />
      {children}
      <Footer />
    </div>
  );
};

export default Layout;
