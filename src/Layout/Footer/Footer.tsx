import React from 'react';

import './footerStyles.css';

const Footer = () => {
  return (
    <footer className="footer">
      <h6 className="footer__title">footer</h6>
    </footer>
  );
};

export default Footer;
