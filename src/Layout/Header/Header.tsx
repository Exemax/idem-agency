import React from 'react';

import './headerStyles.css';

const Header = () => {
  return (
    <header className="header">
      <h1 className="header__logo">header</h1>
    </header>
  );
};

export default Header;
